package techies.vaap.saas.shakelistenerandroid;

import android.content.Context;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.seismic.ShakeDetector;

public class MainActivity extends AppCompatActivity implements ShakeDetector.Listener{

TextView text;
    int i = 0;
    Vibrator v;
   
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView)findViewById(R.id.counts);
        text.setText(""+i);
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        ShakeDetector sd = new ShakeDetector(this);
        sd.start(sensorManager);
    }

    @Override
    public void hearShake() {
        //Toast.makeText(getApplicationContext(),"SHAKED",Toast.LENGTH_LONG).show();
        i++;
       //  v.vibrate(pattern,500);
       v.vibrate(500);
        text.setText(""+i);
    }
}

/** REFER URL
 *  http://stackoverflow.com/questions/14837265/on-shake-action-generate-on-android-app?lq=1
 *  https://github.com/square/seismic/
 *
 * **/